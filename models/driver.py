from google.appengine.ext import db


class Driver(db.Model):
    surname = db.StringProperty(required=True)
    forename = db.StringProperty(required=True)
    nickname = db.StringProperty()
    portrait = db.URLProperty()